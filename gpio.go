// Package gpio provides an interface to general purpose I/O lines.
package gpio

const (
	// HIGH gpio level
	HIGH = 1
	// LOW gpio level
	LOW = 0
)

type Direction string

const (
	In  Direction = "in"
	Out           = "out"
)

type Pin interface {
	// SetDirection sets the direction for the pin
	SetDirection(d Direction) error
	// Read reads the current value of the pin
	Read() (int, error)
	// Write sets the value of the pin
	Write(int) error
}
