package gpio

import "testing"

func TestDio(t *testing.T) {
	ff := NewFakeSysFs()
	SetSysFs(ff)

	d, err := NewDio(42)
	if err != nil {
		t.Fatal(err)
	}

	err = d.SetDirection(Out)
	if err != nil {
		t.Fatal(err)
	}

	err = d.Write(HIGH)
	if err != nil {
		t.Fatal(err)
	}

	x, err := d.Read()
	if x != HIGH {
		t.Errorf("Bad ping value; expected %d, got %d", HIGH, x)
	}
}

func TestLed(t *testing.T) {
	ff := NewFakeSysFs()
	SetSysFs(ff)

	d, err := NewLed("foo")
	if err != nil {
		t.Fatal(err)
	}

	err = d.SetDirection(Out)
	if err != nil {
		t.Fatal(err)
	}

	err = d.Write(HIGH)
	if err != nil {
		t.Fatal(err)
	}

	x, err := d.Read()
	if x != HIGH {
		t.Errorf("Bad ping value; expected %d, got %d", HIGH, x)
	}
}
