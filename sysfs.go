package gpio

import (
	"fmt"
	"strconv"
	"time"

	"github.com/pkg/errors"
)

const (
	gpioPath = "/sys/class/gpio"
	ledPath  = "/sys/class/led"
)

// Dio represents a digital I/O line controlled through the Linux kernel
// sysfs GPIO interface.
type Dio struct {
	pin              int
	value, direction string
}

// Dio represents a digital output line controlled through the Linux kernel
// sysfs LED interface.
type Led struct {
	name, value string
}

func tryRead(path string, attempts int, interval time.Duration) (err error) {
	for attempts > 0 {
		time.Sleep(interval)
		_, err := fs.ReadFile(path)
		if err == nil {
			return err
		}
		attempts--
	}

	return err
}

func NewDio(pin int) (d Dio, err error) {
	d = Dio{
		pin:       pin,
		value:     fmt.Sprintf("%s/gpio%d/value", gpioPath, d.pin),
		direction: fmt.Sprintf("%s/gpio%d/direction", gpioPath, d.pin),
	}

	// Check if pin has been exported ...
	err = tryRead(d.direction, 1, 0)

	if err != nil {
		// Pin needs to be exported
		err = fs.WriteFile(gpioPath+"/export", []byte(d.String()))
		if err != nil {
			return d, err
		}

		// Allow some time for the export process to finish
		err = tryRead(d.direction, 10, time.Millisecond*10)
		if err != nil {
			err = errors.Wrap(err, "export failed")
		}
	}

	return d, err
}

// String implements the Stringer interface
func (d Dio) String() string {
	return strconv.Itoa(d.pin)
}

func (d Dio) SetDirection(dir Direction) (err error) {
	return fs.WriteFile(d.direction, []byte(dir))
}

func (d Dio) Write(val int) (err error) {
	return fs.WriteFile(d.value, []byte(strconv.Itoa(val)))
}

func (d Dio) Read() (val int, err error) {
	b, err := fs.ReadFile(d.value)
	if err != nil {
		return val, err
	}
	return strconv.Atoi(string(b[0]))
}

func NewLed(name string) (l Led, err error) {
	l = Led{
		name:  name,
		value: fmt.Sprintf("%s/%s/brightness", ledPath, name),
	}

	return l, err
}

func (l Led) SetDirection(dir Direction) (err error) {
	return nil
}

func (l Led) Write(val int) (err error) {
	return fs.WriteFile(l.value, []byte(strconv.Itoa(val)))
}

func (l Led) Read() (val int, err error) {
	b, err := fs.ReadFile(l.value)
	if err != nil {
		return val, err
	}
	return strconv.Atoi(string(b[0]))
}
