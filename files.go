package gpio

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"

	"github.com/pkg/errors"
)

type SysFs interface {
	WriteFile(path string, b []byte) error
	ReadFile(path string) ([]byte, error)
}

type NativeSysFs struct{}

// Default filesystem singleton
var fs SysFs = NativeSysFs{}

func SetSysFs(f SysFs) {
	fs = f
}

func (nf NativeSysFs) WriteFile(path string, b []byte) (err error) {
	f, err := os.OpenFile(path, os.O_WRONLY, 0664)
	if err != nil {
		return err
	}
	defer f.Close()
	_, err = f.Write(b)
	return err
}

func (nf NativeSysFs) ReadFile(path string) ([]byte, error) {
	return ioutil.ReadFile(path)
}

type FakeSysFs struct {
	files map[string][]byte
}

func NewFakeSysFs() FakeSysFs {
	return FakeSysFs{files: make(map[string][]byte)}
}

func (ff FakeSysFs) WriteFile(path string, b []byte) (err error) {
	ff.files[path] = make([]byte, len(b))
	copy(ff.files[path], b)
	if path == gpioPath+"/export" {
		n, err := strconv.Atoi(string(b))
		if err == nil {
			ff.files[fmt.Sprintf("%s/gpio%d/direction", gpioPath, n)] = []byte(In)
			ff.files[fmt.Sprintf("%s/gpio%d/value", gpioPath, n)] = []byte("0")
		}
	}
	return nil
}

func (ff FakeSysFs) ReadFile(path string) ([]byte, error) {
	b, found := ff.files[path]
	if !found {
		return nil, errors.New("not found")
	}
	return b, nil
}
